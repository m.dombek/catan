import React from 'react';

import Board from './Components/Board/Board'
import Menu from './Components/Menu/Menu'

import './App.css'

function App() {
  return (
    <div className="App">
      <div className="board-container">     
        <Board></Board>
      </div>
      <div className="menu-container"> 
        <Menu></Menu>
      </div>
    </div>
  );
}

export default App;
