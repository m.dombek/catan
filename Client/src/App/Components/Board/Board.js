import React, { useEffect } from 'react';
import { setBoard, setIsLoading } from '../../../Redux/Board/actionCreators'
import { useSelector, useDispatch } from 'react-redux'
import Hexagon from '../Hexagon/Hexagon'
import CircularProgress from '@material-ui/core/CircularProgress';

import './Board.css'

export default function Board() {
    const dispatch = useDispatch();
    const pjson = require('../../../../package.json')
    const url = pjson.serverUrl;
    
    const boardValues = useSelector(state => state.boardReducer.boardValues);
    const isLoading = useSelector(state => state.boardReducer.isLoading);

    const refreshBoard = () => {

    }

    useEffect(() => {
        fetch(url + "/generateBoard", {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' }
        })
        .then(response => response.json())
        .then(data => {
            dispatch(setBoard(data));
            dispatch(setIsLoading(false));
        },
        [dispatch]);
    }, [])

    return ( 
        <div>      
            { isLoading ? <CircularProgress className="spinner" /> : 
            <div>
                <div className="first-board-row">                    
                    <Hexagon row={1} value={boardValues[0]}></Hexagon>
                    <Hexagon row={1} value={boardValues[1]}></Hexagon>
                    <Hexagon row={1} value={boardValues[2]}></Hexagon>
                </div>
                <div className="second-board-row">
                    <Hexagon row={2} value={boardValues[3]}></Hexagon>
                    <Hexagon row={2} value={boardValues[4]}></Hexagon>
                    <Hexagon row={2} value={boardValues[5]}></Hexagon>
                    <Hexagon row={2} value={boardValues[6]}></Hexagon>
                </div>
                <div className="third-board-row">
                    <Hexagon row={3} value={boardValues[7]}></Hexagon>
                    <Hexagon row={3} value={boardValues[8]}></Hexagon>
                    <Hexagon row={3} value={boardValues[9]}></Hexagon>
                    <Hexagon row={3} value={boardValues[10]}></Hexagon>
                    <Hexagon row={3} value={boardValues[11]}></Hexagon>
                </div>
                <div className="fourth-board-row">
                    <Hexagon row={4} value={boardValues[12]}></Hexagon>
                    <Hexagon row={4} value={boardValues[13]}></Hexagon>
                    <Hexagon row={4} value={boardValues[14]}></Hexagon>
                    <Hexagon row={4} value={boardValues[15]}></Hexagon>
                </div>
                <div className="fifth-board-row">
                    <Hexagon row={5} value={boardValues[16]}></Hexagon>
                    <Hexagon row={5} value={boardValues[17]}></Hexagon>
                    <Hexagon row={5} value={boardValues[18]}></Hexagon>
                </div>
            </div>
            } 
        </div>
    )
}