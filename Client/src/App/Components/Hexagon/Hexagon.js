import React from 'react';
import { useSelector } from 'react-redux'

import './Hexagon.css'

export default function Hexagon(props) {
    let numberContainerClassName = "";    
    let showIntersections = useSelector(state => state.boardReducer.showIntersections   );

    numberContainerClassName += props.value.resource.localeCompare("DESERT") ? " hexagon-number-container " : "";
    numberContainerClassName += (props.value.number === 6 || props.value.number === 8) ? " red-text" : "";

    return (    
        <div className="hexagon-container">
            <div class={ "hexagon " + props.value.resource.toLowerCase() }>
                <div class="hexTop"></div>
                {
                    showIntersections &&
                    <div className="intersections-container"> 
                        <div className="intersection-button top-left-x"></div>
                        <div className="intersection-button top-mid-x"></div>
                        <div className="intersection-button top-right-x"></div>
                        <div className="intersection-button bottom-left-x"></div>
                        <div className="intersection-button bottom-mid-x"></div>
                        <div className="intersection-button bottom-right-x"></div>
                    </div>
                }
                <div class={numberContainerClassName}>                    
                    {props.value.number}
                </div>
                <div class="hexBottom"></div>
            </div>           
        </div>
    )
}