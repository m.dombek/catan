import React from 'react';

import './Test.css';

import Hexagon from '../Hexagon/Hexagon'

export default function Test() {
    return (
        <div>            
            <div className="hello">Hello</div>
            <div>Hello</div>
            <Hexagon className="hexagon-woo"></Hexagon>
            <Hexagon className="hexagon-woo"></Hexagon>
        </div>
    )
}