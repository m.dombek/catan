import React, { useEffect } from 'react';
import { setBoard, setIsLoading, toggleIntersections } from '../../../Redux/Board/actionCreators'
import { useSelector, useDispatch } from 'react-redux'
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';

export default function SimpleMenu() {
    const dispatch = useDispatch();
    const [anchorEl, setAnchorEl] = React.useState(null);
    const pjson = require('../../../../package.json')
    const url = pjson.serverUrl;

    let bestLocation = [];

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const handleShuffleBoard = () => {
        fetch(url + '/generateBoard', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' }
        })
        .then(response => response.json())
        .then(data => {
            dispatch(setBoard(data));
            dispatch(setIsLoading(false));    
            setAnchorEl(null);
        },
        [dispatch]);
    };

    const handleFindBestLocation = () => {
        fetch(url + '/getBestLocation', {
            method: 'POST',
            header: { 'Content-Type': 'application/json' }
        })
        .then(response => response.json())
        .then(data => {
            bestLocation = data.toString();
            alert("Best Location: " + bestLocation.toString())
        })
        
        setAnchorEl(null);
    }

    const handleToggleIntersections = () => {
        dispatch(toggleIntersections());
        setAnchorEl(null);
    }

    return (
        <div>
            <Button variant="contained" color="primary" aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick}>
                Menu
            </Button>
            <Menu
                id="simple-menu"
                anchorEl={anchorEl}
                keepMounted
                open={Boolean(anchorEl)}
                onClose={handleClose}
            >
                <MenuItem onClick={handleFindBestLocation}>Find Best Location</MenuItem>
                <MenuItem onClick={handleShuffleBoard}>Shuffle Board</MenuItem>
                <MenuItem onClick={handleToggleIntersections}>Toggle Intersections</MenuItem>
            </Menu>
        </div>
    );
}