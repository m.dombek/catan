import { combineReducers, createStore } from 'redux';
import boardReducer from './Board/reducer'

const rootReducer = combineReducers({
    boardReducer
});

const store = createStore(rootReducer, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());

export { store }