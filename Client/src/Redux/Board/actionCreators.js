import { SET_BOARD, SET_IS_LOADING, TOGGLE_INTERSECTIONS } from './actionTypes'

export const setBoard = boardValues => ({
    type: SET_BOARD,
    boardValues
})

export const setIsLoading = isLoading => ({
    type: SET_IS_LOADING,
    isLoading
})

export const toggleIntersections = () => ({
    type: TOGGLE_INTERSECTIONS    
})