export const SET_BOARD = "SET_BOARD";
export const SET_IS_LOADING = "SET_IS_LOADING";
export const TOGGLE_INTERSECTIONS = "TOGGLE_INTERSECTIONS"