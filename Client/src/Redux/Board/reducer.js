import { SET_BOARD, SET_IS_LOADING, TOGGLE_INTERSECTIONS } from './actionTypes'

const initialState = {
    boardValues: [],
    isLoading: true,
    showIntersections: false
}

export default function BoardReducer(state = initialState, action) {
    switch (action.type) {
        case SET_BOARD: {
            return {
                ...state,
                boardValues: action.boardValues
            }
        }
        case SET_IS_LOADING: {
            return {
                ...state,
                isLoading: action.isLoading
            }
        }
        case TOGGLE_INTERSECTIONS: {
            return {
                ...state,
                showIntersections: !state.showIntersections
            }
        }
        default: {
            return state
        }
    }
}