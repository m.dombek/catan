Welcome to the Settlers of Catan Simulator!

I made this project out of curiosity to discover how difficult
making a Settlers of Catan game could be.

The project is seperated into two folders: Client and Server.
To run it, use "npm start" in the Client directory,
and "node index.js" in the Server directory.