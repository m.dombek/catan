const express = require('express')
const app = express()
var cors = require('cors');
var bodyParser = require('body-parser')

const pjson = require('./package.json')
const port = pjson.port;
const url = pjson.url;

app.use(express.static(`${__dirname}/client`));

app.use(cors({
    origin: "http://localhost:3000"
}));

app.listen(port, () => console.log(`Example app listening at ${url}:${port}`));

// Constants
const NUM_OF_HEXAGONS = 19;
const RESOURCE_TYPES = require('./constants').RESOURCE_TYPES;
const NUMBERS_SEQUENCE = require('./constants').NUMBERS_SEQUENCE;
const HEXAGON_SEQUENCE = require('./constants').HEXAGON_SEQUENCE;
const NUMBERS_PROBABILITY = require('./constants').NUMBERS_PROBABILITIES;
const POSSIBLE_LOCATIONS = require('./constants').POSSIBLE_LOCATIONS;
const POSSIBLE_RESOURCE_TYPES = require('./constants').POSSIBLE_RESOURCE_TYPES

// Globals
var boardValues = [];

app.get('/', (req, res) => res.send('Hello World!'));

/**
 * Generates the Catan Board.
 * Called when the running the Client.
 */
app.post('/generateBoard', (req, res) => { 
    let isDesertPlaced = 0;
    let availableResources = POSSIBLE_RESOURCE_TYPES.slice();

    for (let hexIndexOnBoard = 0; hexIndexOnBoard < NUM_OF_HEXAGONS; hexIndexOnBoard++) {
        let resourceIndex = Math.floor(Math.random() * availableResources.length); // Index of randomly selected Index
        let resource = availableResources[resourceIndex]; // Resource according to resource index

        // Check if the current resource is a Desert
        if (resource.localeCompare(RESOURCE_TYPES.DESERT) === 0) {
            boardValues[HEXAGON_SEQUENCE[hexIndexOnBoard]] = { resource };
            availableResources.splice(resourceIndex, 1);
            isDesertPlaced = 1;

            continue;
        }        

        // Add the current resource with a number value to the array
        boardValues[HEXAGON_SEQUENCE[hexIndexOnBoard]] = {
            resource,
            number: NUMBERS_SEQUENCE[hexIndexOnBoard - isDesertPlaced]
        }

        // Remove the used resource from the resources pool
        availableResources.splice(resourceIndex, 1);
    }

    res.send(boardValues)
});

app.post('/getBestLocation', (req, res) => {
    let maxRating = 0;
    let bestLocation = [];

    // Go over every possible location
    for (let i = 0; i < POSSIBLE_LOCATIONS.length; i++) {
        let currRating = 0;
        let currLocation = POSSIBLE_LOCATIONS[i];
        let currLocUniqueResources = [];

        // Go over every location's hexagon
        for (let j = 0; j < currLocation.length; j++) {
            let currHexagon = boardValues[currLocation[j]];

            // If the current hexagon isn't a Desert, add it's number value to the rating
            if (currHexagon.resource.localeCompare(RESOURCE_TYPES.DESERT) != 0) {
                currRating += NUMBERS_PROBABILITY[currHexagon.number];

                // If the current resource doesn't already exist in the unique resources pool,
                // Then add it to the array
                if (currLocUniqueResources.indexOf(currHexagon.resource) == -1) {
                    currLocUniqueResources.push(currHexagon.resource);
                }
            }
        }

        // Multiply the location's rating according to the number of unique
        // resources it provides
        currRating *= currLocUniqueResources.length;

        if (currRating > maxRating) {
            maxRating = currRating;
            bestLocation = currLocation;
        }
    }

    res.send(bestLocation);
});

app.post('/getIntersectionsData', (req, res) => {
    let intersectionsData = [];


});

/**
 * Calculates the value of every resource.
 * The calculation is done by rating each resource according to it's rarity on the board.
 * The lower the odds of receiving that resource, the higher it's value.
 */
function calculateResourceValue() {

}

function getHexagonNeighbors(index) {
    var neighbors = new Set();

    
}