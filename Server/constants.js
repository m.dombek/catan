module.exports.HEXAGON_SEQUENCE = [ 0, 1, 2, 6, 11, 15, 18, 17, 16, 12, 7, 3, 4, 5, 10, 14, 13, 8, 9 ]; 
module.exports.NUMBERS_SEQUENCE = [ 5, 2, 6, 3, 8, 10, 9, 12, 11, 4, 8, 10, 9, 4, 5, 6, 3, 11 ]
module.exports.NUMBERS_PROBABILITIES = {
    2: 0.028,
    3: 0.056,
    4: 0.083,
    5: 0.111,
    6: 0.139,
    8: 0.139,
    9: 0.111,
    10: 0.083,
    11: 0.056,
    12: 0.028
};

let RESOURCE_TYPES = module.exports.RESOURCE_TYPES = {
    DESERT: "DESERT",
    CLAY: "CLAY",
    WOOD: "WOOD",
    SHEEP: "SHEEP",
    GRAIN: "GRAIN",
    IRON: "IRON"
}

module.exports.POSSIBLE_RESOURCE_TYPES = [
    RESOURCE_TYPES.DESERT,
    RESOURCE_TYPES.CLAY, 
    RESOURCE_TYPES.CLAY, 
    RESOURCE_TYPES.CLAY, 
    RESOURCE_TYPES.WOOD,
    RESOURCE_TYPES.WOOD,
    RESOURCE_TYPES.WOOD,
    RESOURCE_TYPES.WOOD,
    RESOURCE_TYPES.SHEEP, 
    RESOURCE_TYPES.SHEEP, 
    RESOURCE_TYPES.SHEEP, 
    RESOURCE_TYPES.SHEEP, 
    RESOURCE_TYPES.GRAIN, 
    RESOURCE_TYPES.GRAIN, 
    RESOURCE_TYPES.GRAIN, 
    RESOURCE_TYPES.GRAIN, 
    RESOURCE_TYPES.IRON, 
    RESOURCE_TYPES.IRON, 
    RESOURCE_TYPES.IRON
];

module.exports.POSSIBLE_LOCATIONS = [
    [0, 1, 4],
    [0, 3, 4],
    [1, 4, 5],
    [1, 2, 5],
    [2, 5, 6],
    [3, 7, 8],
    [4, 8, 9],
    [4, 5, 9],
    [5, 9, 10],
    [5, 6, 10],
    [6, 10, 11],
    [7, 8, 12],
    [8, 12, 13],
    [8, 9, 13],
    [9, 13, 14],
    [9, 10, 14],
    [10, 14, 15],
    [10, 11, 15],
    [12, 13, 16],
    [13, 16, 17],
    [13, 14, 17],
    [14, 17, 18],
    [14, 15, 18]
]